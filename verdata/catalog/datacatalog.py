# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.app.textfield import RichText
from plone.directives import form
from zope.interface import Invalid
from Products.CMFCore.utils import getToolByName
from plone.batching import Batch
from verdata.catalog import VerdataCatalogMessageFactory as _

class IDataCatalog(form.Schema):
    """A contact card.
    """

    extendedDescription = RichText(
        title=_(u"Extended Description"),
        required=False,
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    The associated template is found in datacatalog_templates/view.pt.
    """

    grok.context(IDataCatalog)
    grok.require('zope2.View')
    grok.name('view')

from plone.dexterity.content import Container

class DataCatalog(Container):
    """The container for datasets.
    """

    def datasets(self, request):
        catalog = getToolByName(self, 'portal_catalog')
        datasets = map(lambda b: b.getObject(),
                       catalog(portal_type='verdata.Dataset',
                               review_state="published"))

        return Batch(datasets, 8, int(request.get('b_start', 0)), orphan=1)
