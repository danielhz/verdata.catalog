# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.app.textfield import RichText
from plone.directives import form
from zope.interface import Invalid
from plone.app.textfield import RichText
from plone.dexterity.content import Container
from verdata.catalog import VerdataCatalogMessageFactory as _

class IDataVisualization(form.Schema):
    """An RDF dataset.
    """

    jscode = schema.Text(
        title=_(u"Javascript code for visualization"),
        required=True
    )

    extendedDescription = RichText(
        title=_(u"DataVisualization extended description"),
        required=False,
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in datavisualization_templates/view.pt.
    """
    
    grok.context(IDataVisualization)
    grok.require('zope2.View')
    grok.name('view')

class DataVisualization(Container):
    """The object for data visualizations
    """

    def queries(self):
        """The queries
        """
        items = []
        for brain in self.getFolderContents():
            items.append(brain.getObject())
        return items
