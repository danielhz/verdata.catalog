from zope.i18nmessageid import MessageFactory
VerdataCatalogMessageFactory = MessageFactory('verdata.catalog')

def initialize(context):
    """Initializer called when used as a Zope 2 product."""
