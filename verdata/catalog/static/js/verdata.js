
function sparql(query, endpoint, fun) {
    jQuery.ajax({
	url: endpoint,
	dataType: "jsonp",
	data: { query: query, output: "json" }
    }).done(fun);
}

function datasets(endpoint, element) {
    sparql(
        // Query
        "SELECT distinct ?g (count(*) as ?q) " +
            "WHERE { GRAPH ?g { ?s ?p ?o } } " +
            "GROUP BY ?g " +
            "ORDER BY DESC(?q)",
        // Endpoint
        endpoint,
        // Action
        function(data) {
	    $.each(data.results.bindings, function(index, value) {
	        element.append(
		    $('<tr>')
		        .append($('<td>').append(
			    $('<a>')
			        .attr('href',value.g.value.replace('http://verdata.cl/','') + 'index.html')
			        .text(value.g.value)))
		        .append($('<td>').text(value.q.value))
	        );
	    });
        });
}

(function($) {
     $(document).ready(function() {
         $('table.verdata-namedgraphs').each(function() {
             datasets($(this).data('verdata-endpoint'), $(this));
         });
     })
})(jQuery);

