# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.app.textfield import RichText
from plone.directives import form
from zope.interface import Invalid
from plone.app.textfield import RichText
from plone.dexterity.content import Container
from verdata.catalog import VerdataCatalogMessageFactory as _

class IDataTool(form.Schema):
    """A data tool.
    """

    extendedDescription = RichText(
        title=_(u"DataTool extended description"),
        required=False,
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in datatool_templates/view.pt.
    """
    
    grok.context(IDataTool)
    grok.require('zope2.View')
    grok.name('view')

class DataTool(Container):
    """The object for data tools
    """

    def files(self):
        """The files associated with the dataset
        """
        items = []
        for brain in self.getFolderContents():
            items.append(brain.getObject())
        return items

    def fileSize(self, file):
        size = file.get_size()
        if size / 10**9 > 0:
            return "%0.1f GB" % (float(size) / 10**9)
        elif size / 10**6 > 0:
            return "%0.1f MB" % (float(size) / 10**6)
        elif size / 10**3 > 0:
            return "%0.1f KB" % (float(size) / 10**3)
        else:
            return "%i B" % size
