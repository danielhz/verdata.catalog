# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.app.textfield import RichText
from plone.directives import form
from zope.interface import Invalid
from plone.app.textfield import RichText
from plone.dexterity.content import Item
from verdata.catalog import VerdataCatalogMessageFactory as _

class ISPARQLQuery(form.Schema):
    """A SPARQL query.
    """

    sparqlquery = schema.Text(
        title=_(u"SPARQL code for visualization"),
        required=True
    )

    extendedDescription = RichText(
        title=_(u"Extended description"),
        required=False,
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in sparqlquery_templates/view.pt.
    """
    
    grok.context(ISPARQLQuery)
    grok.require('zope2.View')
    grok.name('view')

class SPARQLQuery(Item):
    """The object for SPARQL queries
    """

    def queries(self):
        """The queries
        """
        items = []
        for brain in self.getFolderContents():
            items.append(brain.getObject())
        return items
