# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.app.textfield import RichText
from plone.directives import form
from zope.interface import Invalid
from plone.app.textfield import RichText
from plone.dexterity.content import Item
from verdata.catalog import VerdataCatalogMessageFactory as _

class INamedGraph(form.Schema):
    """An RDF dataset.
    """

    extendedDescription = RichText(
        title=_(u"NamedGraph extended description"),
        required=False,
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in namedgraph_templates/view.pt.
    """
    
    grok.context(INamedGraph)
    grok.require('zope2.View')
    grok.name('view')

class NamedGraph(Item):
    """The object for RDF datasets
    """
