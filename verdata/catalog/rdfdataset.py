# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.app.textfield import RichText
from plone.directives import form
from zope.interface import Invalid
from plone.app.textfield import RichText
from plone.dexterity.content import Container
from verdata.catalog import VerdataCatalogMessageFactory as _

class IRDFDataset(form.Schema):
    """An RDF dataset.
    """

    serviceURL = schema.TextLine(
        title=_(u"Service URL"),
        required=True,
    )

    extendedDescription = RichText(
        title=_(u"RDFDataset extended description"),
        required=False,
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in rdfdataset_templates/view.pt.
    """
    
    grok.context(IRDFDataset)
    grok.require('zope2.View')
    grok.name('view')

class RDFDataset(Container):
    """The object for RDF datasets
    """
    
    def namedgraphs(self):
        """The graphs inside the RDF dataset
        """
        graphs = []
        for brain in self.getFolderContents():
            graphs.append(brain.getObject())
        return graphs
